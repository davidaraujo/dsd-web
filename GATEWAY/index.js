const express = require('express')
const httpProxy = require('express-http-proxy')

const app = express()
const porta = 8000

const produtoServiceProxy = httpProxy("http://127.0.0.1:8080/produtos")
const lojaServiceProxy = httpProxy("http://127.0.0.1:3000/lojas")

app.get('/', (req,res) => res.send("API GATEWAY"))

app.get('/produtos', (req, res) => produtoServiceProxy(req,res))
app.get('/lojas', (req,res) => lojaServiceProxy(req,res))

app.listen(porta, () => console.log(`Gateway rodando na porta ${porta}`))