const express = require("express")
const app = express()
const porta = 8080

const produtosJson = require("./produtos.js")

app.get('/produtos', (req, res) => res.send(produtosJson))
app.listen(porta, () => console.log(`Api rodando na porta ${porta}`))