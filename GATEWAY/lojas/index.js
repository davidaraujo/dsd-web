const express = require("express")
const app = express()
const porta = 3000

const lojasJson = require("./lojas.js")

app.get('/lojas', (req, res) => res.send(lojasJson))
app.listen(porta, () => console.log(`Api rodando na porta ${porta}`))