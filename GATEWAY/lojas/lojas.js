module.exports = {
	"lojas_list":[
		{
			"id":0,
			"nome":"Loja eletronica",
			"endereco":{
				"rua": "Rua fulano",
				"cep": 99999999
			}
		},
		{
			"id":1,
			"nome":"Loja 99",
			"endereco":{
				"rua": "Rua anonimo",
				"cep": 11111111
			}
		},
		{
			"id":2,
			"nome":"Loja tem de tudo",
			"endereco":{
				"rua": "Rua sicrano",
				"cep": 22222222,
			}
		}
	]
}
