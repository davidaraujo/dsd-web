#include "conversor.h"

void
convert_prog_1(char * host, long num, char *base){
	CLIENT *clnt;
	long *result_1;
	values conv_dec_1_arg;
	long *result_2;
	values conv_bin_1_arg;
#ifndef DEBUG
	clnt = clnt_create(host, CONVERT_PROG, CONVERT_VERSION, "udp");
	if(clnt == NULL){
		clnt_pcreateerror(host);
		exit(1);
	}
#endif /* DEBUG */
	if(*base == 'd'){
		conv_dec_1_arg.num = num;
		result_1 = conv_dec_1(&conv_dec_1_arg, clnt);
		if(result_1 == (long *) NULL){
			clnt_perror(clnt, "call failed");
		}else{
			printf("\n");
			printf("Número em binário: %ld\n", *result_1);
			printf("\n");
		}
	}else if(*base == 'b'){
		conv_bin_1_arg.num = num;
		result_2 = conv_bin_1_(&conv_bin_1_arg, clnt);
		if(result_2 == (long *) NULL){
			clnt_perror(clnt, "call failed");
		}else{
			printf("\n");
			printf("Número em decimal: %ld\n", *result_2);
			printf("\n");
		}
	}
#ifndef DEBUG
	clnt_destroy(clnt);
#endif /* DEBUG */
}


int
main(int argc, char *argv[]){
	char *host;
	if(argc < 4){
		printf("Usage: %s server_host\n", argv[0]);
		exit(1);
	}
	host = argv[1];
	convert_prog_1(host, atoi(argv[2]), argv[3]);
exit(0);
}