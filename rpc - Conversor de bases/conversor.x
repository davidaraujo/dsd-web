struct values{
	long num;
	char base;
}

program CONVERSOR_PROG{
	version CONVERSOR_VERSION{
		long conv_dec(values) = 2;
		long conv_bin(values) = 1;
	} = 1;
} = 554;