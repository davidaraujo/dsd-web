
#include "conversor.h"
#include <stdio.h>

char * reverter(char arr[], int tam){
	char var_temp;
	int t,i = 0, inicio = 0, fim = tam - 1;
	for(t = 0; i < tam / 2; t++){
		var_temp = arr[fim];
		arr[fim] = arr[inicio];
		arr[inicio] = var_temp;
		inicio++;
		fim--;
	}

	return arr;
}

long *
conv_dec_1_svc(values *argp, struct svc_req *rqstp){
	static long result;
	long resto = 0, num_c = argp -> num;
	char arr[32];
	int i = 0;
	while(num_c > 0){
		resto = num_c % 2;
		arr[i] = resto + '0';
		num/=2;
		i++;
	}
	result = atoi(reverter(arr,i));
	return &result;
}

long * 
conv_bin_1_svc(values *argp, struct svc_req *rqstp){
	static long result;
	long resto = 0, soma = 0, num_c = argp -> num_c;
	int base = 1;
	while(num_c > 0){
		resto = num_c % 10;
		if(resto > 0){
			soma += base;
		}
		base*=2;
		num_c/=10;
	}
	result = soma;
	return & result;
}







