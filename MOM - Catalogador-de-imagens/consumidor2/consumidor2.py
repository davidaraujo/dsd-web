import pika
import os
import uuid

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='imagens', exchange_type='topic')

result = channel.queue_declare(queue = '', exclusive=True)
queue_name = result.method.queue

binding_key = 'imgs_coloridas'

channel.queue_bind(exchange='imagens', queue=queue_name, routing_key=binding_key)

def retorno(ch, method, props, body):
	write_local = os.path.join("salvar_imgs_col", "img_col_%s.jpg"%str(uuid.uuid4()))
	with open(write_local, "wb") as img:
		img.write(body)
	print("Imagem recebida")
 
channel.basic_consume(queue = queue_name, auto_ack=True, on_message_callback = retorno)

print("Aguardando envio de imagens...")
channel.start_consuming()