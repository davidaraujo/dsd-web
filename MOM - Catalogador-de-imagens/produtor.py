import pika
import sys

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.exchange_declare(exchange = 'imagens', exchange_type='topic')

print("##Catalogue imagens##")
img_arquivo = input("Digite o nome do arquivo de imagem(com extensão):")
tipo = int(input("Tipo de imagem:\n(1)Preto e branco\n(2)Colorida\n"))

if(tipo == 1):
	routing_key = 'imgs_preto_e_branco' 
elif(tipo == 2):
	routing_key = 'imgs_coloridas' 
else:
	print("Opcao invalida")
	sys.exit(1)

try:
	with open(img_arquivo, "rb") as img:
		data = img.read()
except IOError:
	print("Arquivo nao acessivel")
	sys.exit(1)

channel.basic_publish(exchange = 'imagens', routing_key = routing_key, body=data)
print("Imagem enviada")

connection.close()