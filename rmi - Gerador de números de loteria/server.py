import Pyro4 
import random

@Pyro4.expose 
class Loteria:
	def __init__(self):
		self.pares = False
		self.impares = False

	def set_pares(self, b):
		self.pares = b

	def set_impares(self, b):
		self.impares = b
		
	def gerar_numeros(selfm, num_max, quant_min ,quant_max, quant):
		num_gerados = []
		quant = max(min(quant, quant_max), quant_min)
		while quant > 0:
			num = random.randint(1, num_max)
			if self.pares and num % 2 > 0:
				continue
			elif self.impares and num % 2 == 0:
				continue
			if num not in num_gerados:
				num_gerados.append(num)
				quant-=1
		num_gerados.sort()
		return "".join(str(num_gerados).replace(',',' - '))
	

try:
	daemon = Pyro4.Daemon() 
	name_server = Pyro4.locateNS()
	uri = daemon.register(Loteria) 
	name_server.register('Loteria', uri) 
	print("Aguardando...")
	daemon.requestLoop()
except Exception as ex:
	print("Erro: " + str(ex))




