import Pyro4

def tipo_jogo(tipo):
	num_max, quant_min, quant_max = 0, 0, 0
	if tipo == 1:
		num_max, quant_min, quant_max = 60, 6, 15
	elif tipo == 2:
		num_max, quant_min, quant_max = 25, 15, 20
	elif tipo == 3:
		num_max, quant_min, quant_max = 80, 5, 15
	else:
		num_max, quant_min, quant_max = 60, 6, 15
		print("Jogo não selecionado, megasena definido como padrão")
	return [num_max, quant_min,quant_max]

def gerador_nums(loteria,tipos,quant):
	while 1:
		print("\n"+loteria.gerar_numeros(tipos[0],tipos[1],tipos[2],quant)+"\n")
		gerar = int(input("Gerar novamente:(1)Sim (2)Não\n"))
		if gerar == 1:
			continue
		elif gerar == 2:
			break;
		else:
			break;

def main():
	try:
		name_server = Pyro4.locateNS()
		uri = name_server.lookup('Loteria')
		loteria = Pyro4.Proxy(uri)
		print("##GERADOR DE NÚMEROS DE LOTERIA##\n")
		tipo = int(input("Escolha o tipo de jogo:\n(1)Megasena (2)Lotofacil (3)Quina\n"))
		tipos = tipo_jogo(tipo)
		filtro = int(input("Filtrar por números:\n(1)Pares (2)Impares (Outro)Todos\n"))
		if filtro == 1:
			loteria.set_pares(True)
		elif filtro == 2:
			loteria.set_impares(True)
		quant = int(input("Gerar quantos números:"))
		gerador_nums(loteria,tipos,quant)
		print("Programa finalizado")
	except Exception as ex:
		print("Erro: " + str(ex))

main()



