import socket

endereco = 'localhost'
porta = 8080

def conexao(endereco, porta):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((endereco, porta))
    except Exception as ex:
        print("Erro de conexão") 
    return s

def client():
    client_socket = conexao(endereco,porta)

    try:
        print("========Cifra de césar========")
        print("Escreva uma mensagem e uma chave(1 - 26)")
        while 1:
            msg = input("Mensagem: ")
            chave = input("Chave: ")
            if(not(msg)):
                print("Mensagem em branco")
                print("")
                continue
            elif not(int(chave) >= 1 and int(chave) <= 26):
                print("Chave inválida")
                print("")
                continue     
            client_socket.send(msg.encode())
            client_socket.send(chave.encode())  
            data = client_socket.recv(2048)
            print("Mensagem Criptografada: " + data.decode())
            print("")
    except Exception as ex:
        print("Erro: %s"%str(ex))
    finally: 
        print("Programa encerrado")
        client_socket.close()

client()