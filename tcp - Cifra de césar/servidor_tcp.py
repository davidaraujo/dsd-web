import socket

endereco = 'localhost'
porta = 8080

def criptografar(msg,chave):
	arr = ""
	for i in range(0, len(msg)):
		codigo = ord(msg[i])
		if((codigo >= 65 and codigo <= 90) or (codigo >= 97 and codigo <= 122)):
			soma = codigo + chave
			res = soma - 26 if (soma > 122 or (soma > 90 and msg[i].isupper())) else soma 
			arr+=chr(res)
		else:
			arr+=msg[i]
	return arr

def conexao(endereco, porta):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((endereco, porta)) 
	s.listen(1) 
	return s

def servidor():
	server_socket = conexao(endereco,porta)
	while 1: 
	    print ("Esperando conexão do cliente")
	    client, address = server_socket.accept() 
	    while 1:
	    	msg = client.recv(2048).decode() 
	    	chave = client.recv(2048).decode()
	    	chave = int(chave)
	    	client.send(criptografar(msg,chave).encode())

servidor()

