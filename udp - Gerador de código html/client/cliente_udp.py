import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
	print("Gerador de código html")
	titulo = input("Informe um titulo para a página: ")
	print("Envolva as palavras com '_' para italico ou '*' para bold")
	p = input("Informe um paragráfo:\n")
	client_socket.sendto(titulo.encode(),("localhost", 8080))
	client_socket.sendto(p.encode(),("localhost", 8080))
	data,endereco = client_socket.recvfrom(2048)
	f = open("index.html", 'wb')
	try:
		while data:
			f.write(data)
			client_socket.settimeout(2)
			data,endereco = client_socket.recvfrom(2048)
	except Exception:
		print("Html gerado com sucesso")
		f.close()
except Exception as e:
	print("Erro: " + str(e))
finally:
	client_socket.close()

