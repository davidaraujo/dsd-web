import socket
import os

host = "localhost"
porta = 8080

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind((host,porta))

server.listen()

cabecalho = "<!DOCTYPE html>\n<html>\n<head>\n<title>Index</title>\n</head>\n<body>\n"

def compilarParagrafo(p):
	msg = ""
	bold,italic = False,False
	for i in range(0, len(p)):
		if p[i] == "*":
			if(bold == False):
				msg += "<b>"
				bold = True
			else:
				msg += "</b>"
				bold = False                   
		elif p[i] == "_":
			if(italic == False):
				msg += "<i>"
				italic = True
			else:
				msg += "</i>"
				italic = False         
		else:
			msg += p[i]
	return msg
       

def gerarCodigo(titulo, paragrafo):
	f = open('index.html', 'w')
	f.write(cabecalho)
	f.write(f"<h1>{titulo}</h1>\n")
	f.write(f"<p>{compilarParagrafo(paragrafo)}</p>\n")
	f.write("</body>\n</html>")
	f.close()

while 1:
	try:
		print("Aguardando requisição")
		data, endereco = server_socket.recvfrom(2048)
		data2, endereco = server_socket.recvfrom(2048)
		gerarCodigo(data.decode(), data2.decode())
		f = open('index.html', 'rb')
		data = f.read(2048)
		while data:
			if(server_socket.sendto(data,(host, endereco[1]))):
				data = f.read(2048)
		f.close()
		os.remove("index.html")
	except Exception as e:
		print("Erro: " + str(e))
		break;

