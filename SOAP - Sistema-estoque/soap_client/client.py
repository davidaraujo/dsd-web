from zeep import Client

def templateImpressao(iden, nome, codigo, quant, d_cadastro):
	print("\n##############################")
	print("Id produto: " + str(iden))
	print("Nome produto: " + nome)
	print("Codigo produto: " + codigo)
	print("Quantidade produto: " + str(quant))
	print("Data cadastro: " + d_cadastro)
	print("##############################\n")


def funcListarProdutos(client):
	produtos = client.service.listarProdutos()
	if(produtos):
		for prod in produtos:
			templateImpressao(prod.id ,prod.nome, prod.codigo, prod.quantidade, prod.data_cadastro)
	else:
		print("\n>>Estoque ainda não possui produtos\n")


def funcInserirProd(client):
	nome = str(input("Nome do produto: "))
	cod_prod = str(input("Codigo produto: "))
	quantidade = int(input("Quantidade: "))
	try:
		client.service.inserirProduto(nome, cod_prod, quantidade)
		print("\n>>Produto inserido com sucesso\n")
	except:
		print("\n>>Erro ao inserir produto\n")
	print("")


def funcBuscarProduto(client):
	prod_pesq = str(input("Digite o codigo do produto: "))
	prod = client.service.buscarProduto(prod_pesq)
	if(prod.nome and prod.codigo):
		templateImpressao(prod.id ,prod.nome, prod.codigo, prod.quantidade, prod.data_cadastro)
	else:
		print("\n>>Produto pesquisado não existe no estoque\n")


def funcDeletarProduto(client):
	ind = int(input("Digitar indice do produto: "))
	quant = int(input("Quantidade a excluir: "))
	prod_del = client.service.deletarProduto(ind, quant)
	if(prod_del):
		print("\n>>Produtos(s) deletado(s) com sucesso\n")
	else:
		print("\n>>Produto não deletado. Informe um índice válido\n")


def main():
	client = Client("http://127.0.0.1:8080/estoque?wsdl")
	print(">>>>SISTEMA DE ESTOQUE<<<<")
	while True:
		print("(1)Listar produtos\n(2)Inserir produto\n(3)Buscar produto\n(4)Deletar produto\n(5)Limpar estoque")
		menu = int(input("Informe número da opção: "))
		if(menu == 1):
			funcListarProdutos(client)
		elif(menu == 2):
			funcInserirProd(client)
		elif(menu == 3):
			funcBuscarProduto(client)
		elif(menu == 4):
			funcDeletarProduto(client)
		elif(menu == 5):
			client.service.limparEstoque()
			print("\n>>Estoque limpado com sucesso\n")
		else:
			print("\nOpcao inválida\nPrograma encerrado")
			break

main()

