
package soapestoque;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface InterfaceEstoque {
    
    @WebMethod public ProdutoArrayList listarProdutos();
    @WebMethod public void inserirProduto(String nome, String codigo, int quant);
    @WebMethod public Boolean deletarProduto(int produto_id, int quant);
    @WebMethod public Produto buscarProduto(String nome);
    @WebMethod public void limparEstoque();
    
}


