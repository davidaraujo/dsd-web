
package soapestoque;

import java.time.LocalDate;

public class Produto {
    
    private int id;
    private String nome;
    private String codigo;
    private int quantidade;
    private final LocalDate data = LocalDate.now();
    private String data_cadastro;
    
    public Produto(int id, String nome, String cod, int quant){
        this.id = id;
        this.nome = nome;
        this.codigo = cod;
        this.quantidade = quant;
        this.data_cadastro = data.toString();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getData_cadastro() {
        return data_cadastro;
    }

    public void setData_cadastro(String data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
