package soapestoque;

import javax.jws.WebService;

import java.util.ArrayList;

@WebService(endpointInterface = "soapestoque.InterfaceEstoque")
public class ServicoEstoque implements InterfaceEstoque{

    private ArrayList<Produto> produtos = new ArrayList<>();   
    private int count_id = 0;
    
    @Override
    public Boolean deletarProduto(int produto_id, int quant) {  
        int i = 0;
        for(Produto prod: this.produtos){            
            if(produto_id == prod.getId()){      
                int quant_prod = prod.getQuantidade();
                if((quant_prod  - quant) > 0){
                    this.produtos.get(i).setQuantidade(quant_prod - quant);            
                }else{
                    this.produtos.remove(produto_id);
                }
                return true;
            }
            i++;
        }  
        return false;
    }

    @Override
    public Produto buscarProduto(String codigo) {    
        for(Produto prod : this.produtos){
            if(prod.getCodigo().equals(codigo)){
                return prod;
            }
        }    
        return new Produto(0,null,null,0);
    }

    @Override
    public void inserirProduto(String nome, String codigo, int quant) {
        Produto prod = new Produto(this.count_id ,nome, codigo, quant);
        this.produtos.add(prod);
        this.count_id++;
    }

    @Override
    public ProdutoArrayList listarProdutos() {
        ProdutoArrayList wrapper = new ProdutoArrayList();
        wrapper.setList(this.produtos);
        return wrapper;
    }

    @Override
    public void limparEstoque() {
        this.produtos.clear();
    }
    
}

